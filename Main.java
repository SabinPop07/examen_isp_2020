public class Main {
    public static void main(String[] args) {
        D d;
        System.out.println("ok");
    }
}
interface I {
}


class B {
    public B(long t, C c) {
        this.t = t;
        this.c = c;
    }

    public B() {
    }

    long t;
    C c;
    void b(){
        c = new C();
        c.metB(this);
    }
}

class C {
    public C() {
    }

    void metB(B b)
    {

    }
}


class D extends B{
    public D(G g, F f) {
        this.g = g;
        this.f = f;
    }

    public D() {
    }

    G g;
    F f;
    void met1(int i)
    {
        g = new G();
        f = new F();
        f.n("asd");
        g.met3();
    }
}

class E extends D{
    public E() {
    }

    @Override
    void met1(int i) {
        super.met1(i);
    }
}

class F {
    public F() {
    }

    void n(String s)
    {

    }
}

class G {
    public G() {
    }

    double met3()
    {
        double x=3.00;
        return x;
    }
}
