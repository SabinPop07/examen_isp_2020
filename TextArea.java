import javax.swing.*;

public class TextArea extends JFrame {
    JTextArea zone1, zone2;
    JButton button;

    TextArea() {
        setTitle("Text Area");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(450, 300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 200, height = 150;

        zone1 = new JTextArea();
        zone1.setBounds(10, 10, width, height);

        zone2 = new JTextArea();
        zone2.setBounds(10 + width + 10, 10, width, height);

        button = new JButton("Convert");
        button.setBounds(10, 10 + height + 10, width*2+10, 80);

        button.addActionListener(l -> {
            zone2.setText("");
            int length = zone1.getText().length();
            // ia in considerare toate caracterele, inclusiv spatiile si newline
            for(int i = 0; i < length; i++) {
                zone2.append("a");
            }
        });

        add(zone1); add(zone2); add(button);
    }

    public static void main(String[] args) { new TextArea(); }

}
